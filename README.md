# _Hilos_C

## Ejemplo
* Supongamos que ejecutamos este ejemplo con 5 Productores y 1 Consumidor con un Buffer de Tamaño 2, ==> Cuál podría ser el escenario de salida?
	* Un escenario podría ser que se ejecuten 2 Productores, 1 Consumidor y finalmente un 1 Productor; quedando a la espera 2 Productores ya que el buffer se encuentra lleno y ya no hay nadie más que pueda consumir.
	* Entonces el escenario en este caso mostraría que el programa no termina (Sin embargo, podemos afirmar que es correcta la ejecución).

## Notas
* No olvidar crear el directorio "out_make" en la raiz del repositorio, es decir en "_Hilos_C" ya que es donde el "make" alojará el ejecutable. 
