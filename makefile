DIR_OUT = out_make
SRC = src

all:
	gcc -Wall -g -c $(SRC)/FIFO_main_threads.c -o $(DIR_OUT)/FIFO_main_threads.o 
	gcc -Wall -g -o $(DIR_OUT)/FIFO_main_threads $(DIR_OUT)/FIFO_main_threads.o -lpthread

clean:
	$(RM) -f $(DIR_OUT)/*
