// FIFO_main_threads
/*This implementation is appropriate when produces ans consumer are main*/
/*
* wait = -1
* signal = +1
*/

/*
Escenario 1:
	* Entra primero en el Get
	* CurrentSize = 0 - 1 = -1 (Queda bloqueado, sigue quedando en 0)
	* Entra entonces al Put
	* RoomLeft = FIFOSIZE - 1 = 9
	* FIFOmutex = 1 - 1 = 0 (Entra)
	* Hace proceso
	* FIFOmutex = 0 + 1 = 1 (Libera)
	* CurrentSize = 0 + 1 = 1 (Un dato agregado)
*/

/*
* Objects declared as volatile are omitted from optimization.
* Para funciones con "static" al principio las vuelve privadas.
* A static int variable remains in memory while the program is running.
*/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

/* compilacion condicional
*  En 0 no muestra los mensajes, en 1 es el caso contrario.
*/
#define __DEBUG 1

#define M 1
#define N 50
#define FIFOSIZE 2
int volatile *PutPt;  // put next
int volatile *GetPt;  // get next
int static Fifo[FIFOSIZE];
int volatile contador;
int volatile hilosEjecutados;
int volatile primerHilo;
pthread_mutex_t FIFOmutex;    // exclusive acces to FIFO
sem_t  CurrentSize;  // 0 means FIFO empty
sem_t RoomLeft;     // 0 means FIFO full

void OS_Fifo_Init(void);
void *productor(void *);
void *consumidor(void *);
void OS_Fifo_Put(int id);
void OI_Fifo_Get(int id);

int main(int argc, char const *argv[]){
	if(argc<2){
		printf("<CONSUMIDORES> <PRODUCTORES>\n");
    	return 1;
  	}
	int CONSUMIDORES = atoi(argv[1]);
    int PRODUCTORES = atoi(argv[2]);

	pthread_t producer[PRODUCTORES], consumer[CONSUMIDORES];
	printf("Total de Productores [%d]\n", PRODUCTORES);
	printf("Total de Consumidores [%d]\n", CONSUMIDORES);
	printf("Con un Buffer de tamaño [%d]\n", FIFOSIZE);

	OS_Fifo_Init();

	for(int i = 0; i < PRODUCTORES; ++i){
		pthread_create(&producer[i], NULL, productor, (void *) &i);	
		sleep(1);  // Es importante 	
	}

	for(int i = 0; i < CONSUMIDORES; ++i){
		pthread_create(&consumer[i], NULL, consumidor, (void *) &i);
		sleep(1);  // Es importante 	
	}

	for(int i = 0; i < PRODUCTORES; ++i){
		pthread_join(producer[i], NULL);
	}

	for(int i = 0; i < CONSUMIDORES; ++i){
		pthread_join(consumer[i], NULL);
	}

    pthread_mutex_destroy(&FIFOmutex); /* Free up the_mutex */
    (void)sem_destroy(&CurrentSize);
    (void)sem_destroy(&RoomLeft);
    printf("\t--> Total de Hilos ejecutados [%d]\n", hilosEjecutados);
	return 0;
}

void *productor(void *params){
	int id;
	id = *(int *) params;
	OS_Fifo_Put(id);
	pthread_exit(NULL);
}

void *consumidor(void *params){
	int id;
	id = *(int *) params;
	OI_Fifo_Get(id);
	pthread_exit(NULL);
}

void OS_Fifo_Init(void){
	srand (time(NULL));
	PutPt = GetPt = &Fifo[0];  // Empty
	contador = 0;
	hilosEjecutados = 0;
	primerHilo = 1;
	/*
    	CurrentSize = 0
    	RoomLeft = FIFOSIZE
    	FIFOmutex = 1
    */
    sem_init(&CurrentSize, 0, 0);
    sem_init(&RoomLeft, 0, FIFOSIZE);
    pthread_mutex_init(&FIFOmutex, NULL); // Lo inicializa en 1 (Desbloqueado)
}

void OS_Fifo_Put(int id){  
	sem_wait(&RoomLeft);  // RoomLeft - 1 (Debe entrar desde un principio porque RoomLeft = FIFOSIZE)
	#if __DEBUG
		if(primerHilo){
			printf("\t -->ESTE MENSAJE DEBE SIEMPRE SER EL PRIMERO...\n");
		}
		primerHilo = 0;
	#else
		//
	#endif
	pthread_mutex_lock(&FIFOmutex); // FIFOmutex - 1 
	#if __DEBUG
		printf("\t -->Entra el productor [%d]...\n", id);
	#else
		//
	#endif

	int data = rand () % (N-M+1) + M;   // Este está entre M y N

	*(PutPt) = data;       // Put
	PutPt++;               // Place to put next
	contador++;
	printf("Productor [%d] ha producido [%d], contador = [%d]\n", id, data, contador);
	if(PutPt == &Fifo[FIFOSIZE]){
		PutPt = &Fifo[0];     // wrap
		#if __DEBUG
			printf("\t\tEl hilo Productor [%d] ha llegado al final del Buffer, se apunta el puntero al Principio.\n", id);
		#else
			//
		#endif
	}
	hilosEjecutados++;

	#if __DEBUG
		printf("\t -->Sale el productor [%d]...\n", id);
	#else
		//
	#endif
	pthread_mutex_unlock(&FIFOmutex); // FIFOmutex + 1 
	sem_post(&CurrentSize); // CurrentSize + 1
}

void OI_Fifo_Get(int id){
	int data;

	sem_wait(&CurrentSize); // CurrentSize - 1 (Se queda Bloqueado desde un principio porque CurrentSize = 0)
	pthread_mutex_lock(&FIFOmutex);  // FIFOmutex - 1 
	#if __DEBUG
		printf("\t -->Entra el consumidor [%d]...\n", id);
	#else
		//
	#endif

	data = *(GetPt);    // get data
	GetPt++;         // points to next data to get
	contador--;
	printf("Consumidor [%d] ha consumido [%d], contador = [%d]\n", id, data, contador);
	if(GetPt == &Fifo[FIFOSIZE]){
		GetPt = &Fifo[0];   // wrap
		#if __DEBUG
			printf("\t\tEl hilo Consumidor [%d] ha llegado al final del Buffer, se apunta el puntero al Principio.\n", id);
		#else
			//
		#endif
	}
	hilosEjecutados++;

	#if __DEBUG
		printf("\t -->Sale el consumidor [%d]...\n", id);
	#else
		//
	#endif
	pthread_mutex_unlock(&FIFOmutex); // FIFOmutex + 1 
	sem_post(&RoomLeft);  // RoomLeft + 1
}